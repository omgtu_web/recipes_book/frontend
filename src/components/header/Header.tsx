import React from 'react';
import { actions, selectors, useAppDispatch, useAppSelector } from '../../store';
import './header.css';

const Header = () => {
  const user = useAppSelector(selectors.auth.getUser);
  const dispatch = useAppDispatch();

  return (
    <header>
      <h2>Книга рецептов</h2>
      <div className='userBlock'>
        {user?.username}
        <button onClick={() => dispatch(actions.auth.logout())}>Выйти</button>
      </div>
    </header>
  );
};

export default Header;
