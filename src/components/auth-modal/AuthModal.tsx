import React, { useState } from 'react';
import { actions, selectors, useAppDispatch, useAppSelector } from '../../store';
import './auth-modal.css';
import { AuthType } from '../../common/enums';

const AuthModal: React.FC = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [authType, setAuthType] = useState<AuthType>(AuthType.SIGNIN);
  const user = useAppSelector(selectors.auth.getUser);
  const dispatch = useAppDispatch();

  const changeAuthType = () => {
    if (authType === AuthType.SIGNIN) {
      setAuthType(AuthType.SIGNUP);
    }
    if (authType === AuthType.SIGNUP) {
      setAuthType(AuthType.SIGNIN);
    }
  };

  return (
    <div className={!user ? 'modal_view active' : 'modal_view'}>
      <div className={!user ? 'modal_view_content active' : 'modal_view_content'}>
        <h3>{authType === AuthType.SIGNIN ? 'Авторизация' : 'Регистрация'}</h3>
        <button className='changeAuthType' type="button" onClick={changeAuthType}>
          {authType === AuthType.SIGNIN ? 'Хотите зарегистрироваться?' : 'Хотите авторизироваться?'}
        </button>

        <form>
          <input onChange={(event) => setUsername(event.target.value)} placeholder="Username" />
          <input onChange={(event) => setPassword(event.target.value)} placeholder="Password" type={'password'} />
          <button type="button" onClick={() => {
            if (authType === AuthType.SIGNIN) {
              dispatch(actions.auth.signIn({ username, password }))
            }
            if (authType === AuthType.SIGNUP) {
              dispatch(actions.auth.signUp({ username, password }))
            }
          }}>
            Ввод
          </button>
        </form>
      </div>
    </div>
  );
};

export default AuthModal;