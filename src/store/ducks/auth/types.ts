import { UserModel } from "../../../common/types";

export interface UserState {
  user: UserModel | null;
  jwt: string | null;
}