import { createAsyncThunk } from "@reduxjs/toolkit";
import { api } from "../../../api";
import { DefaultRejectValue, ExtraParamsThunkType, SignInDto, SignUpDto, UserModel } from "../../../common/types";
import { UserState } from "./types";

export const signIn = createAsyncThunk<
  UserState,
  SignInDto,
  ExtraParamsThunkType<DefaultRejectValue>
>('user/signIn', async function (dto: SignInDto, { rejectWithValue }) {
  try {
    const { data } = await api.post("users/sign-in", dto);

    return data;
  } catch (error) {
    return rejectWithValue(error as DefaultRejectValue);
  }
});

export const signUp = createAsyncThunk<
  UserState,
  SignInDto,
  ExtraParamsThunkType<DefaultRejectValue>
  >('user/signUp', async function (dto: SignUpDto, { rejectWithValue }) {
    try {
      const { data } = await api.post("users/sign-up", dto);
  
      return data;
    } catch (error) {
      return rejectWithValue(error as DefaultRejectValue);
    }
  });

export const getMe = createAsyncThunk<
  any,
  undefined,
  ExtraParamsThunkType<DefaultRejectValue>
>('user/getMe', async function (_, { rejectWithValue }) {
  try {
    const { data } = await api.get("users/me");

    return data;
  } catch (error) {
    return rejectWithValue(error as DefaultRejectValue);
  }
});