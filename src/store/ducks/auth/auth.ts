import { createSlice } from "@reduxjs/toolkit";

import { UserModel } from "../../../common/types";
import { getMe, signIn, signUp } from "./actions";
import { UserState } from "./types";


const initialState: UserState = {
  user: null,
  jwt: localStorage.getItem("jwt"),
};

const authSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    logout: (state) => {
      state.user = {} as UserModel;
      state.jwt = '';
      localStorage.removeItem("jwt");
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(signIn.fulfilled, (state, { payload }) => {
        state.jwt = payload.jwt;
        state.user = payload.user;
        if (payload.jwt) {
          localStorage.setItem("jwt", payload.jwt);
        }
      })
      .addCase(signUp.fulfilled, (state, { payload }) => {
        state.jwt = payload.jwt;
        state.user = payload.user;
        if (payload.jwt) {
          localStorage.setItem("jwt", payload.jwt);
        }
      })
      .addCase(getMe.fulfilled, (state, { payload }) => {
        state.user = payload;
      })
      .addCase(getMe.rejected, (state) => {
        state.jwt = null;
        state.user = null;
      })
  }
});

export const actions = { ...authSlice.actions, signIn, signUp, getMe };

export const reducer = authSlice.reducer;
