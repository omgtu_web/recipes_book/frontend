import React, { useEffect } from 'react';

import { Header, AuthModal, Recipes } from './components';
import { actions, useAppDispatch } from './store';

const App: React.FC = () => {
  const dispatch = useAppDispatch();
  
  useEffect(() => {
    dispatch(actions.auth.getMe());
  }, []);

  return (
    <div className="App">
      <Header />
      <Recipes />
      <AuthModal />
    </div>
  );
};

export default App;
