export type SignInDto = {
  username: string;
  password: string;
}

export type SignUpDto = SignInDto;

export type AuthModel = {
  jwt: string;
  user: UserModel;
}

export type UserModel = {
  id: string;
  username: string;
}

export type ExtraParamsThunkType<T> = {
  rejectValue: T;
};

export type DefaultRejectValue = {
  message: string;
  error: string;
  statusCode: number;
};


