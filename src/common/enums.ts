export enum AuthType {
  SIGNIN = 'sign-in',
  SIGNUP = 'sign-up',
}