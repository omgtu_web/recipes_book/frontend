import axios from "axios";

import { actions, store } from "../store";

const BASE_URL = "http://localhost:5000/";

export const api = axios.create({
  baseURL: BASE_URL,
});
api.interceptors.request.use((config) => {
  const jwt = store.getState().auth.jwt;
 
  if (!jwt) {
    return config;
  }

  const Authorization = `Bearer ${jwt}`;

  return {
    ...config,
    headers: { ...config.headers, Authorization },
  };
});
api.interceptors.response.use(
  (response) => {
    if (response.status === 401) {
      store.dispatch(actions.auth.logout());
    }
    return response;
  },
  (error) => {
    if (error.response.status && error.response.status === 401) {
      store.dispatch(actions.auth.logout());
    }
    return Promise.reject(error.response.data);
  }
);